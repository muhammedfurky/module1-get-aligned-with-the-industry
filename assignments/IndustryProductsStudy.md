**Product 1 Name :**
PhotoVerify

**Product 1 link :**
https://www.bioid.com/identity-proofing-photoverify/

**Product 1 Short description :**
It performs a highly accurate automated online ID photo and live photo comparison.

**Product 1 is combination of features :**
ID Recognition (Face recognition) and Person detection (Liveness detection)

**Product 1 is provided by which company?**
BioID

**Product 2 Name :**
AI-LOCK Plus

**Product 2 link :**
https://guardianva.com/ai-lock-plus

**Product 2 Short description :**
A door lock system with a powerful facial recognition software that allows the user to create “whitelist” of authorized personnel for secure access control.

**Product 2 is combination of features :**
ID Recognition (Face recognition)

**Product 2 is provided by which company?**
Guardian Systems